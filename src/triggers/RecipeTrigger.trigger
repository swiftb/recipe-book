/**
 * Created by bswif on 3/1/2021.
 */

trigger RecipeTrigger on Recipe__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    Recipe_Book_Setting__mdt theSettings = RecipeBook_UTIL.theSettings;

    //block changes pending edits
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            Map<Id,Recipe__c> theRecipes = new Map<Id,Recipe__c>([SELECT Id, (SELECT Id FROM Recipe_Edits__r WHERE Id NOT IN :Trigger.new) FROM Recipe__c WHERE ID IN :Trigger.new]);

            for(Recipe__c theRec : Trigger.new){
                Recipe__c oldRec = Trigger.oldMap.get(theRec.Id);
                if(theRecipes.get(theRec.Id).Recipe_Edits__r.size()>0 && oldRec.Status__c == 'Awaiting Edit Approval'){
                    theRec.addError(Label.Recipe_Trigger_EditsPending);
                }
            }
        }

        //set status on external user dml
        if(Trigger.isInsert){
            if(theSettings.Require_Approval_on_External_Updates__c && (UserInfo.getUserType() != 'Standard' || RecipeBook_UTIL.isExt)){
                List<Recipe__c> parentRecipes = new List<Recipe__c>();
                try{
                    for(Recipe__c theRec : Trigger.new){
                        if(theRec.Editing_Recipe__c != null){
                            parentRecipes.add(new Recipe__c(Id = theRec.Editing_Recipe__c,Status__c = 'Awaiting Edit Approval'));
                        }
                    }
                    update parentRecipes;
                }catch(Exception e){
                    throw new RecipeBook_UTIL.RecipeBookException(Label.RecipeException_ExternalStatusError);
                }
            }
        }
    }

    if(Trigger.isAfter){
        //handle approvals if required
        if(Trigger.isInsert){
            if(theSettings.Require_Approval_on_External_Updates__c && (UserInfo.getUserType() != 'Standard' || RecipeBook_UTIL.isExt)){
                try{
                    List<Approval.ProcessSubmitRequest> theApps = new List<Approval.ProcessSubmitRequest>();
                    for(Recipe__c theRec : Trigger.new){
                        Approval.ProcessSubmitRequest theApp = new Approval.ProcessSubmitRequest();
                        theApp.setObjectId(theRec.Id);
                        theApp.setSubmitterId(UserInfo.getUserId());
                        theApp.setSubmitterId(theSettings.Approval_User_Id__c);
                        if(theRec.Editing_Recipe__c == null){
                            theApp.setProcessDefinitionNameOrId(theSettings.External_New_Approval_Process_API_Name__c);
                            RecipeBook_UTIL.approveNew = true;
                            RecipeBook_UTIL.approveEdit = false;
                        }else{
                            theApp.setProcessDefinitionNameOrId(theSettings.External_Edit_Approval_Process_API_Name__c);
                            RecipeBook_UTIL.approveEdit = true;
                            RecipeBook_UTIL.approveNew = false;
                        }
                        theApps.add(theApp);
                    }
                    if(!Test.isRunningTest()){
                        Approval.process(theApps);
                    }
                }catch (Exception e){
                    throw new RecipeBook_UTIL.RecipeBookException(Label.RecipeException_ApprovalError);
                }
            }
        }

        //handle approval process finish
        if(Trigger.isUpdate){
            Set<String> approvalStatuses = new Set<String> {'Published','Edit Rejected'};
            for(Recipe__c theRec : Trigger.new){
                if(!RecipeEditClass.isRunning && Trigger.oldMap.get(theRec.Id).Status__c != theRec.Status__c && approvalStatuses.contains(theRec.Status__c)){
                    RecipeEditClass.isRunning = true;
                    if(theRec.Status__c == 'Edit Rejected'){
                        RecipeEditClass.rejectEdit(theRec.Id);
                    }else if(Trigger.oldMap.get(theRec.Id).Status__c == 'Awaiting Edit Approval'){
                        RecipeEditClass.acceptEdit(theRec.Id);
                    }
                }
            }
        }
    }

}