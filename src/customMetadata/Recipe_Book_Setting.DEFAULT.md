<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DEFAULT</label>
    <protected>false</protected>
    <values>
        <field>Approval_User_Id__c</field>
        <value xsi:type="xsd:string">0055Y00000EEjmD</value>
    </values>
    <values>
        <field>External_Edit_Approval_Process_API_Name__c</field>
        <value xsi:type="xsd:string">Approve_Recipe_Edits</value>
    </values>
    <values>
        <field>External_New_Approval_Process_API_Name__c</field>
        <value xsi:type="xsd:string">Approve_New_Recipe</value>
    </values>
    <values>
        <field>Recipe_Scraper_Access_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Recipe_Scraper_Endpoint__c</field>
        <value xsi:type="xsd:string">https://web-recipe-scraper.herokuapp.com/getrecipe/</value>
    </values>
    <values>
        <field>Require_Approval_on_External_Updates__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
