<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Recipe_Status_Awaiting_Approval</fullName>
        <description>Set the Status to Awaiting Approval.</description>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Recipe Status: Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recipe_Status_Awaiting_Edit_Approval</fullName>
        <description>Set the Status to Awaiting Edit Approval.</description>
        <field>Status__c</field>
        <literalValue>Awaiting Edit Approval</literalValue>
        <name>Recipe Status: Awaiting Edit Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recipe_Status_Edit_Rejected</fullName>
        <description>Set the Status to Edit Rejected.</description>
        <field>Status__c</field>
        <literalValue>Edit Rejected</literalValue>
        <name>Recipe Status: Edit Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recipe_Status_Published</fullName>
        <description>Set the Status to Published.</description>
        <field>Status__c</field>
        <literalValue>Published</literalValue>
        <name>Recipe Status: Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recipe_Status_Rejected</fullName>
        <description>Set the Status to Rejected</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Recipe Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
