/**
 * Created by bswif on 2/15/2021.
 */

import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import NAME_FIELD from '@salesforce/schema/Recipe__c.Name';
import INGREDIENTS_FIELD from '@salesforce/schema/Recipe__c.Ingredients__c';
import STEPS_FIELD from '@salesforce/schema/Recipe__c.Recipe_Steps__c';

export default class RecipeDock extends NavigationMixin(LightningElement)  {
    // objectApiName is "Account" when this component is placed on an account record page
    @api objectApiName;
    @track createRecipe = false;

    fields = [NAME_FIELD, INGREDIENTS_FIELD, STEPS_FIELD];

    handleSuccess(event) {
        this.toggleCreate();
        const evt = new ShowToastEvent({
            title: "Recipe created",
            message: "Record ID: " + event.detail.id,
            variant: "success"
        });
        this.dispatchEvent(evt);

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                actionName: 'view',
                recordId : event.detail.id
            }
        });
    }

    toggleCreate(){
        this.createRecipe = !this.createRecipe;
    }
}