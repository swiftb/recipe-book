/**
 * Created by bswif on 2/14/2021.
 */

import { LightningElement, track, api } from 'lwc';

export default class RecipeImgList extends LightningElement {
    @api imgSrcs = [];
    @track theImgs;
    @api isVert;

    connectedCallback(){
        this.theImgs = []
        for(let theSrc of this.imgSrcs){
            let theUrl = `https://${window.location.hostname}/sfc/servlet.shepherd/document/download/`+theSrc.theId;
            this.theImgs.push(
                {
                    theUrl: theUrl,
                    theName: theSrc.theName,
                    theDesc: theSrc.theDesc
                }
            );
        }
    }

    get divClass(){
        return this.isVert === "true"? "vStyle" : "hStyle";
    }

    selectImg(event){
        console.log('in the list');
        console.log(event.detail.theUrl);
        const theEvent = new CustomEvent("selectedimg", {
            detail: {
                theUrl: event.detail.theUrl,
                theName: event.detail.theName,
                theDesc: event.detail.theDesc
            }
        });
        this.dispatchEvent(theEvent);
    }
}