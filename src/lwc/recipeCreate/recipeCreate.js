/**
 * Created by bswif on 2/15/2021.
 */

/* TODO
        //https://salesforce.stackexchange.com/questions/276624/fields-required-at-layout-level-are-not-required-in-lightning-record-form-lwc
        https://developer.salesforce.com/docs/component-library/bundle/lightning-record-form/specification
        //
 */

import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import updateImages from '@salesforce/apex/CTRL_RecipeCreate.updateImages';
import scrapeRecipe from '@salesforce/apex/CTRL_RecipeCreate.scrapeRecipe';
import getSettings from '@salesforce/apex/CTRL_RecipeCreate.getSettings';
import NAME_FIELD from '@salesforce/schema/Recipe__c.Name';
import INGREDIENTS_FIELD from '@salesforce/schema/Recipe__c.Ingredients__c';
import STEPS_FIELD from '@salesforce/schema/Recipe__c.Recipe_Steps__c';
import createSuccessLabel from '@salesforce/label/c.recipeForm_Create_Success';
import createPendingLabel from '@salesforce/label/c.recipeForm_Create_Pending';
import missingSettingLabel from '@salesforce/label/c.recipeForm_Missing_Settings';
import doneLabel from '@salesforce/label/c.Generic_Done';

export default class RecipeCreate extends NavigationMixin(LightningElement) {
    cstmLabels = {missingSettingLabel, doneLabel};

    @api objectApiName;
    fields = [NAME_FIELD, INGREDIENTS_FIELD, STEPS_FIELD];
    @track recordId;
    @track imgFiles;

    @track showRecForm = true;
    @track showRecCreate = true;
    @track showImgUpload = false;
    @track showImgDetails = false;
    @track approvalReq;
    @track showFinal = false;

    @track scrapeUrl;
    @track disableScrape = true;
    @track scrapeSuccess = false;
    @track scrapedRecipe = {};

    @wire(getSettings)
    wiredSettings({ error, data }){
        if(error){
            this.showRecForm = false;
            this.showErr(cstmLabels.missingSettingLabel);
        }else if(data){
            if(data.foundSetting){
                this.approvalReq = data.approvalReq;
            }else{
                this.showRecForm = false;
                this.showErr(cstmLabels.missingSettingLabel);
            }
        }
    }

    get finishMessage(){
        return (this.approvalReq)? createPendingLabel : createSuccessLabel;
    }

    handleScrapeUrlChange(event){
        console.log('updating url');
        this.scrapeUrl = event.target.value;
        this.disableScrape = !(this.scrapeUrl);
    }

    handleScrape(event){
        scrapeRecipe({recUrl: this.scrapeUrl})
        .then(result =>{
            if(result.isSuccess){
                console.log('success');
                this.scrapedRecipe = result.theRecipe;
                console.log(this.scrapedRecipe);
                this.scrapeSuccess = true;
                this.showRecCreate = false;
            }else{
                console.log('bad result');
                console.log(result.errMsg);
                this.showErr(result.errMsg);
            }
        })
        .catch(error => {
            console.log('error');
            console.log(error);
            this.showErr(error.message);
        });
    }

    handleRecipeSubmit(event) {
        console.log('in recipe submit');
        event.preventDefault();
        this.recordId = event.detail.id;
        console.log(this.recordId);
        this.showRecForm = false;
        this.showImgUpload = true;
    }

    handleFileSubmit(event){
        console.log('in file submit');
        event.preventDefault();
        console.log(event.detail.files);


        let theFiles = [];
        for(let i = 0; i < event.detail.files.length; i++){
            let theFile = event.detail.files[i];
            let theUrl = `https://${window.location.hostname}/sfc/servlet.shepherd/document/download/`+theFile.documentId;
            theFiles.push({
                    theUrl: theUrl,
                    Title: theFile.name,
                    Description: "",
                    theIndex: i,
                    Id: theFile.documentId
             });
        }
        console.log("the files");
        console.log(theFiles);

        this.imgFiles = theFiles;
        this.showImgUpload = false;
        this.showImgDetails = true;
    }

    handleFileDetailsSubmit(event){
        console.log('in file detail submit');
        updateImages({objString: JSON.stringify(event.detail), recordId: this.recordId})
        .then(result =>{
            this.showImgDetails = false;
            this.showFinal = true;
        })
        .catch(error => {
            console.log(error);
            this.showErr(error.message);
        });
    }

    handleReload(event){
        this.imgFiles = null;
        this.recordId = null;
        this.showFinal = false;
        this.showRecCreate = true;
    }

    handleCancel(event){
        console.log('cancelling');
        const theEvent = new CustomEvent("cancel", {
        });
        this.dispatchEvent(theEvent);
    }

    get acceptedFormats() {
        return ['.jpg','.jpeg', '.png', '.PNG', '.JPG', '.JPEG'];
    }

    showErr(errMsg){
        const event = new ShowToastEvent({
            title: '',
            variant: error,
            message: errMsg
        });
        this.dispatchEvent(event);
    }

}