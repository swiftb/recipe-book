/**
 * Created by bswif on 8/19/2020.
 */

import { LightningElement, api, track } from 'lwc';

export default class RecipeImgTile extends LightningElement {
    @api theImg;
    @api isVert;
    @api isSingle;

    get cardClass(){
        return this.isVert === "true"? "vStyle" : "hStyle";
    }

    get imgClass(){
        return this.isSingle === "true"? "imgSingle slds-align_absolute-center" : "imgList slds-align_absolute-center";
    }

    selectImg(){
        console.log('in the tile');
        const theEvent = new CustomEvent("selectedimg", {
            detail: this.theImg
        });
        this.dispatchEvent(theEvent);
    }
}