/**
 * Created by bswif on 2/15/2021.
 */

import { LightningElement, api } from 'lwc';

export default class RecipeImgDetails extends LightningElement {
    @api imgDetails;

    updateDetails(event){
        console.log('updating img');

        let placeholderImg = {
            Title: this.imgDetails.Title,
            theUrl: this.imgDetails.theUrl,
            Description: this.imgDetails.Description,
            Id: this.imgDetails.Id,
            theIndex: this.imgDetails.theIndex
        };
        console.log(placeholderImg);

        placeholderImg[event.target.name] = event.target.value;

        const theEvent = new CustomEvent("updatedimg", {
            detail: placeholderImg
        });
        this.dispatchEvent(theEvent);
    }
}