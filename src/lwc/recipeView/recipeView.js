/**
 * Created by bswif on 2/13/2021.
    https://recipesapp-developer-edition.na156.force.com/s/recipe/a005Y00002FdvcfQAB/chilaquiles
 */

import { LightningElement, track, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import recipeGetData from '@salesforce/apex/CTRL_RecipeView.getRecipeData';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class RecipeView extends LightningElement {
    @api recordId;
    @track theRecipe;
    @track popIngredients = false;
    @track popPhotoList = false;
    @track popPhoto = false;
    @track imgSrcs;
    @track recipeLoaded = false;
    @track curImg;

    @wire(recipeGetData, { recordId: '$recordId' })
    wiredRecord({ error, data }){
        if(error){
            //todo error message
            console.log('error');
            console.log(error);
        }else if (data){
            console.log('data');
            console.log(data);
            console.log(data.theRecipe);
            this.theRecipe = data.theRecipe;
            this.imgSrcs = data.theImgs;
            console.log(this.theRecipe);
            this.recipeLoaded = true;
        }
    }

    toggleIngredients(){
        this.popIngredients = !this.popIngredients;
    }

    togglePhotoList(){
        this.popPhotoList = !this.popPhotoList;
    }

    togglePhoto(event){
        this.popPhoto = !this.popPhoto;
        this.curImg = (this.popPhoto? event.detail : null);
        this.popIngredients = false;
        this.popPhotoList = false;
    }

    get modalPop(){
        return this.popPhotoList || this.popIngredients || this.popPhoto;
    }
}