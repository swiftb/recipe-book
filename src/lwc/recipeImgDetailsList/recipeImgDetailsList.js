/**
 * Created by bswif on 2/15/2021.
 */

import { LightningElement, api, track} from 'lwc';

export default class RecipeImgDetailsList extends LightningElement {
    @api srcImgs;
    @track liveImgs = [];

    connectedCallback(){
        if(this.liveImgs.length == 0 && this.srcImgs){
            for(let i = 0; i < this.srcImgs.length; i++){
                this.liveImgs.push(
                    {
                        theUrl:  this.srcImgs[i].theUrl,
                        theIndex: this.srcImgs[i].theIndex,
                        Title: this.srcImgs[i].Title,
                        Description: this.srcImgs[i].Description,
                        Id: this.srcImgs[i].Id
                    }
                );
            }
        }
    }

    handleUpdate(event){
        this.liveImgs[event.detail.theIndex] = event.detail;
    }

    handleSubmit(event){
        console.log('submitting');
        const theEvent = new CustomEvent("updatedimglist", {
            detail: this.liveImgs
        });
        this.dispatchEvent(theEvent);
    }

}