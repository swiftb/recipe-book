/**
 * Created by bswif on 3/7/2021.
 */
/*
    Unit Tests for RecipeTrigger
 */
@IsTest
public with sharing class Recipe_TEST_RecipeTrigger {

    @IsTest
    public static void testUpdateBlock(){
        setupTestData();
        insert testRecipes;
        testRecipes[1].Editing_Recipe__c = testRecipes[0].Id;
        update testRecipes[1];
        testRecipes[0].Status__c = 'Published';

        String errMsg;
        Test.startTest();
        try{
            update testRecipes[0];
        }catch (Exception e){
            errMsg = e.getMessage();
        }
        Test.stopTest();
        System.assert(errMsg.contains(Label.Recipe_Trigger_EditsPending));
    }

    @IsTest
    public static void testStatusSet(){
        setupTestData();
        insert testRecipes;
        RecipeBook_UTIL.isExt = true;
        testRecipes[1].Editing_Recipe__c = testRecipes[0].Id;
        Test.startTest();
        update testRecipes[1];
        Test.stopTest();

        System.assertEquals('Awaiting Edit Approval',[SELECT Status__c FROM Recipe__c WHERE Id = :testRecipes[0].Id].Status__c);
    }

    @IsTest
    public static void testApproveNew(){
        setupTestData();
        RecipeBook_UTIL.isExt = true;
        Test.startTest();
        insert testRecipes[0];
        Test.stopTest();
        System.assert(RecipeBook_UTIL.approveNew);
        System.assert(!RecipeBook_UTIL.approveEdit);
    }

    @IsTest
    public static void testApproveEdit(){
        setupTestData();
        insert testRecipes[0];
        RecipeBook_UTIL.isExt = true;
        testRecipes[1].Editing_Recipe__c = testRecipes[0].Id;
        Test.startTest();
        insert testRecipes[1];
        Test.stopTest();
        System.assert(!RecipeBook_UTIL.approveNew);
        System.assert(RecipeBook_UTIL.approveEdit);
    }

    //Test Accepting and Rejecting Edits (covers RecipeEditClass)
    @IsTest
    public static void testAcceptEdit(){
        setupTestData();
        insert testRecipes;
        setTheLinks();

        testRecipes[1].Editing_Recipe__c = testRecipes[0].Id;
        update testRecipes[1];
        testRecipes[1].Status__c = 'Published';
        Test.startTest();
        update testRecipes[1];
        Test.stopTest();

        List<Recipe__c> checkRecipes = [SELECT Id, Ingredients__c, Recipe_Steps__c, Status__c, Vegan__c FROM Recipe__c];
        System.assertEquals(1,checkRecipes.size());
        System.assertEquals(testRecipes[0].Id,checkRecipes[0].Id);
        System.assertEquals('New Ingredients',checkRecipes[0].Ingredients__c);
        System.assertEquals('The Steps',checkRecipes[0].Recipe_Steps__c);
        System.assertEquals('Published',checkRecipes[0].Status__c);
        System.assertEquals(false,checkRecipes[0].Vegan__c);

        List<ContentDocumentLink> newLinks = [SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId = :testRecipes[0].Id];
        System.assertEquals(2, newLinks.size());
        Set<Id> contentVersions = new Set<Id>{theContents[0].Id,theContents[1].Id};
        System.assertNotEquals(newLinks[0].ContentDocument.LatestPublishedVersionId,newLinks[1].ContentDocument.LatestPublishedVersionId);
        for(ContentDocumentLink theLink : newLinks){
            System.assert(contentVersions.contains(theLink.ContentDocument.LatestPublishedVersionId));
        }

    }

    @IsTest
    public static void testRejectEdit(){
        setupTestData();
        insert testRecipes;
        setTheLinks();
        testRecipes[1].Editing_Recipe__c = testRecipes[0].Id;
        update testRecipes[1];
        testRecipes[1].Status__c = 'Edit Rejected';
        Test.startTest();
        update testRecipes[1];
        Test.stopTest();

        List<Recipe__c> checkRecipes = [SELECT Id, Ingredients__c, Recipe_Steps__c, Status__c, Vegan__c FROM Recipe__c];
        System.assertEquals(1,checkRecipes.size());
        System.assertEquals(testRecipes[0].Id,checkRecipes[0].Id);
        System.assertEquals('Old Ingredients',checkRecipes[0].Ingredients__c);
        System.assertEquals('The Steps',checkRecipes[0].Recipe_Steps__c);
        System.assertEquals('Published',checkRecipes[0].Status__c);
        System.assertEquals(true,checkRecipes[0].Vegan__c);

        List<ContentDocumentLink> newLinks = [SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId = :testRecipes[0].Id];
        System.assertEquals(1, newLinks.size());
        System.assertEquals(theLinks[0].Id,newLinks[0].Id);
    }

    public static List<Recipe__c> testRecipes;
    public static List<ContentVersion> theContents;
    public static List<ContentDocument> theDocs;

    public static void setupTestData(){
        testRecipes = new List<Recipe__c>{
                new Recipe__c(Name = 'Parent', Status__c = 'Awaiting Edit Approval',Ingredients__c = 'Old Ingredients',Recipe_Steps__c = 'The Steps', Vegan__c = true),
                new Recipe__c(Name = 'Child Edit', Status__c = 'Awaiting Edit Approval', Ingredients__c = 'New Ingredients', Vegan__c = false)
        };
        theContents = new List<ContentVersion>{
                new ContentVersion(
                        Title = 'Balloons',
                        PathOnClient = 'Balloons.jpg',
                        VersionData = Blob.valueOf('Balloons'),
                        IsMajorVersion = true
                ),
                new ContentVersion(
                        Title = 'Bubbles',
                        PathOnClient = 'Bubbles.jpg',
                        VersionData = Blob.valueOf('Bubbles'),
                        IsMajorVersion = true
                )
        };
        insert theContents;

        theDocs = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument ORDER BY LatestPublishedVersion.Title ASC];

    }

    public static List<ContentDocumentLink> theLinks;
    public static void setTheLinks(){
        theLinks =  new List<ContentDocumentLink>{
                new ContentDocumentLink(
                        LinkedEntityId = testRecipes[0].Id,
                        ContentDocumentId = theDocs[0].Id,
                        ShareType = 'V',
                        Visibility = 'AllUsers'
                ),
                new ContentDocumentLink(
                        LinkedEntityId = testRecipes[1].Id,
                        ContentDocumentId = theDocs[0].Id,
                        ShareType = 'V',
                        Visibility = 'AllUsers'
                ),
                new ContentDocumentLink(
                    LinkedEntityId = testRecipes[1].Id,
                    ContentDocumentId = theDocs[1].Id,
                    ShareType = 'V',
                    Visibility = 'AllUsers'
                )
            };
        insert theLinks;
    }
}