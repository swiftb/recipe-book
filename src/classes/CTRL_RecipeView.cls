/**
 * Created by bswif on 2/14/2021.
 */

public with sharing class CTRL_RecipeView {
    @AuraEnabled(Cacheable=true)
    public static RecipeData getRecipeData(Id recordId){
        Recipe__c theRecipe = [SELECT Id, Name, Recipe_Steps__c, Ingredients__c, Source__c,
            (SELECT ContentDocumentId, ContentDocument.Title, ContentDocument.Description
                FROM ContentDocumentLinks
                    WHERE ContentDocument.FileType IN ('jpg','jpeg', 'png', 'PNG', 'JPG', 'JPEG')
                    ORDER BY ContentDocument.Title ASC
            )
        FROM Recipe__c WHERE Id = :recordId];

        List<ImageWrapper> theImgs = theRecipe.ContentDocumentLinks.size()>0? new List<ImageWrapper>() : null;
        for(ContentDocumentLink theDoc : theRecipe.ContentDocumentLinks){
            theImgs.add(new ImageWrapper(theDoc.ContentDocumentId,theDoc.ContentDocument.Title, theDoc.ContentDocument.Description));
        }
        return new RecipeData(theRecipe,theImgs);
    }

    public class RecipeData{
        @AuraEnabled
        public Recipe__c theRecipe {get;set;}
        @AuraEnabled
        public List<ImageWrapper> theImgs {get;set;}

        public RecipeData(Recipe__c theRecipeParam, List<ImageWrapper> theImgsParam){
            theRecipe = theRecipeParam;
            theImgs = theImgsParam;
        }
    }

    public class ImageWrapper{
        @AuraEnabled
        public String theId {get;set;}
        @AuraEnabled
        public String theName {get;set;}
        @AuraEnabled
        public String theDesc {get;set;}

        public ImageWrapper(Id theIdParam, String theNameParam, String theDescParam){
            theId = theIdParam;
            theName = theNameParam;
            theDesc = theDescParam;
        }
    }
}