/**
 * Created by bswif on 2/15/2021.
 */


/*
TODO
    err handling for EVERYTHING
 */

public without sharing class CTRL_RecipeCreate {
    public static final String missingEndpointErr = Label.recipeForm_Missing_Endpoint;

    @AuraEnabled(Cacheable = true)
    public static SettingsWrapper getSettings(){
        List<Recipe_Book_Setting__mdt> settingsList = [SELECT Id, Recipe_Scraper_Access_Code__c, Recipe_Scraper_Endpoint__c, Require_Approval_on_External_Updates__c FROM Recipe_Book_Setting__mdt];
        if(settingsList.size() > 0){
            return new SettingsWrapper(settingsList[0].Require_Approval_on_External_Updates__c, true);
        }else{
            return new SettingsWrapper(null, false);
        }
    }

    private static Recipe_Book_Setting__mdt privateSettings;
    public static Recipe_Book_Setting__mdt theSettings{
        get{
            if(privateSettings == null){
                List<Recipe_Book_Setting__mdt> settingsList = [SELECT Id, Recipe_Scraper_Access_Code__c, Recipe_Scraper_Endpoint__c, Require_Approval_on_External_Updates__c FROM Recipe_Book_Setting__mdt];
                privateSettings = settingsList.size() > 0? settingsList[0] : null;
            }
            return privateSettings;
        }set;
    }

    @AuraEnabled
    public static UpdateImgRes updateImages(String objString, String recordId){
        List<ContentDocument> theDocs = (List<ContentDocument>) JSON.deserialize(objString, List<ContentDocument>.class);
        update theDocs;

        List<ContentDocumentLink> theLinks = [SELECT Id, ContentDocumentId, Visibility FROM ContentDocumentLink WHERE ContentDocumentId IN :theDocs AND LinkedEntityId = :recordId];
        for(Integer i = 0; i < theLinks.size(); i++){
            theLinks[i].Visibility = 'AllUsers';
        }
        update theLinks;

        return new UpdateImgRes();
    }

    @AuraEnabled
    public static ScrapeRecipeRes scrapeRecipe(String recUrl){
        try{
            if(theSettings == null){
                return new ScrapeRecipeRes(false,null,missingEndpointErr);
            }else{
                HttpRequest theCallout = new HttpRequest();
                String theEndpoint = theSettings.Recipe_Scraper_Endpoint__c;
                theEndpoint = theEndpoint + '?recipe-url='+recUrl;
                theEndpoint = theSettings.Recipe_Scraper_Access_Code__c != null? theEndpoint + '&code='+theSettings.Recipe_Scraper_Access_Code__c : theEndpoint;
                theCallout.setEndpoint(theEndpoint);
                theCallout.setMethod('GET');
                Http http = new Http();
                HttpResponse theResp = http.send(theCallout);
                Boolean isSuccess = theResp.getStatusCode() == 200;
                String errMsg = theResp.getStatusCode() != 200? theResp.getStatus()+'\n\n'+theResp.getBody() : null;

                Recipe__c theRecipe = isSuccess? parseScrape(theResp.getBody()) : new Recipe__c();
                if(isSuccess){
                    theRecipe.Source__c = recUrl;
                }
                return new ScrapeRecipeRes(isSuccess,theRecipe,errMsg);
            }
        }catch(Exception e){
            return new ScrapeRecipeRes(false,null,e.getMessage());
        }

    }

    public static Recipe__c parseScrape(String theScrape){
        ScrapeObj theObj = (ScrapeObj) JSON.deserialize(theScrape,ScrapeObj.class);

        String theIngredients = '';
        if(theObj.ingredients.size() > 0){
            theIngredients = '<ul>';
            for(String theIngredient : theObj.ingredients){
                theIngredients += '<li>'+theIngredient+'</li>';
            }
            theIngredients += '</ul>';
        }

        String theSteps = '';

        if(theObj.instructions.split('\n').size()>1){
            theObj.instructions = theObj.instructions.replaceAll('\n\n','\n');
            theSteps = '<ol>';
            for(String theStep : theObj.instructions.split('\n')){
                theSteps += '<li>'+theStep+'</li>';
            }
            theSteps += '</ol>';
        }else{
            theSteps = theObj.instructions;
        }

        return new Recipe__c(Name = theObj.title,Ingredients__c = theIngredients, Recipe_Steps__c = theSteps);
    }

    public class UpdateImgRes{

    }

    public class ScrapeObj{
        public String title{get;set;}
        public List<String> ingredients{get;set;}
        public String instructions{get;set;}
    }

    public class ScrapeRecipeRes{
        @AuraEnabled
        public Boolean isSuccess {get;set;}
        @AuraEnabled
        public Recipe__c theRecipe {get;set;}
        @AuraEnabled
        public String errMsg {get;set;}

        public ScrapeRecipeRes(Boolean isSuccessParam, Recipe__c theRecipeParam, String errMsgParam){
            isSuccess = isSuccessParam;
            theRecipe = theRecipeParam;
            errMsg = errMsgParam;
        }

    }

    public class SettingsWrapper{
        @AuraEnabled
        public Boolean approvalReq;
        @AuraEnabled
        public Boolean foundSetting;

        public SettingsWrapper(Boolean approvalReqParam, Boolean foundSettingParam){
            approvalReq = approvalReqParam;
            foundSetting = foundSettingParam;
        }
    }
}