/**
 * Created by bswif on 3/7/2021.
 */

public with sharing class RecipeBook_UTIL {
    private static Recipe_Book_Setting__mdt privateSettings;
    public static Recipe_Book_Setting__mdt theSettings{
        get{
            if(Test.isRunningTest()){
                return new Recipe_Book_Setting__mdt(Approval_User_Id__c = UserInfo.getUserId(),External_New_Approval_Process_API_Name__c = 'New',External_Edit_Approval_Process_API_Name__c='Edit',Require_Approval_on_External_Updates__c=true);
            }else if(privateSettings != null){
                return privateSettings;
            }else{
                try{
                    privateSettings = [SELECT Id, Require_Approval_on_External_Updates__c, External_Edit_Approval_Process_API_Name__c, External_New_Approval_Process_API_Name__c, Approval_User_Id__c FROM Recipe_Book_Setting__mdt LIMIT 1];
                    return privateSettings;
                }catch(Exception e){ 
                    throw new RecipeBookException(Label.RecipeException_SettingsError);
                }
            }
        }set;
    }

    //booleans for unit tests
    @TestVisible
    public static Boolean isExt = false;
    @TestVisible
    public static Boolean approveNew;
    @TestVisible
    public static Boolean approveEdit;

    public class RecipeBookException extends Exception{}
}