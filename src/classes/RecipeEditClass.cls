/**
 * Created by bswif on 2/28/2021.
 */
/*
methods to handle Recipes when they leave Approval Process for editing
 */
public with sharing class RecipeEditClass {
    public static Boolean isRunning = false;
    
    public static void acceptEdit(Id recipeId){
        try{
            Set<String> recFields = new Set<String>();

            Map<String, Schema.SObjectField> recFieldMap = Schema.getGlobalDescribe().get('Recipe__c').getDescribe().fields.getMap();
            for(String theKey : recFieldMap.keySet()){
                recFields.add(theKey);
            }

            String fieldString = String.join(new List<String>(recFields),',');

            String editQuery = 'SELECT '+fieldString+', (SELECT Id, ContentDocumentId FROM ContentDocumentLinks) FROM Recipe__c WHERE Id = \''+recipeId+'\'';
            Recipe__c theEdits = Database.query(editQuery);
            Recipe__c theRec = [SELECT Id, (SELECT Id, ContentDocumentId FROM ContentDocumentLinks) FROM Recipe__c WHERE Id = :theEdits.Editing_Recipe__c];

            //doc ids to check for removal if they don't have any links
            Set<Id> docIds = new Set<Id>();
            for(ContentDocumentLink cdl : theRec.ContentDocumentLinks){
                docIds.add(cdl.ContentDocumentId);
            }

            //delete images (to be reset below)
            if(theRec.ContentDocumentLinks.size() > 0){
                delete theRec.ContentDocumentLinks;
            }

            //insert content document links from edit recipe to the new one
            List<ContentDocumentLink> newCdls = new List<ContentDocumentLink>();
            for(ContentDocumentLink theCdl : theEdits.ContentDocumentLinks){
                newCdls.add(new ContentDocumentLink(ShareType = 'V', Visibility = 'AllUsers',ContentDocumentId = theCdl.ContentDocumentId, LinkedEntityId = theRec.Id));
            }
            insert newCdls;

            //edit the Rec
            for(String theField : recFields){
                DescribeFieldResult theFieldRes = recFieldMap.get(theField).getDescribe();
                if(theField != 'editing_recipe__c' && ((theEdits.get(theField) != null && theEdits.get(theField) != theFieldRes.getDefaultValue()) || theFieldRes.getType() == Schema.DisplayType.BOOLEAN) && theFieldRes.isUpdateable()){
                    theRec.put(theField,theEdits.get(theField));
                }
            }

            //delete the edit rec
            delete theEdits;

            theRec.Status__c = 'Published';
            update theRec;

            //clear content missing cdls
            List<ContentDocument> docsToDelete = new List<ContentDocument>();
            for(ContentDocument theCd : [SELECT Id, (SELECT Id FROM ContentDocumentLinks WHERE ShareType = 'V') FROM ContentDocument WHERE Id IN :docIds]){
                if(theCd.ContentDocumentLinks.size() == 0){
                    docsToDelete.add(theCd);
                }
            }
            delete docsToDelete;
        }catch (Exception e){
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
            throw new RecipeBook_UTIL.RecipeBookException(String.format(Label.RecipeException_EditError,new List<Object>{e.getMessage()+' - '+e.getStackTraceString()}));
        }
    }

    public static void rejectEdit(Id recipeId){
        try{
            Id parentId = [SELECT Id, Editing_Recipe__c FROM Recipe__c WHERE Id = :recipeId].Editing_Recipe__c;
            delete (new Recipe__c(Id = recipeId));
            update (new Recipe__c(Id = parentId, Status__c = 'Published'));
        }catch(Exception e){
            System.debug(e);
            throw new RecipeBook_UTIL.RecipeBookException(String.format(Label.RecipeException_EditError,new List<Object>{e.getMessage()+' - '+e.getStackTraceString()}));
        }
    }
}